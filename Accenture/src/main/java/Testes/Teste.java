package Testes;
import org.junit.*;

public class Teste extends TesteWorkflow
{
    @Before
    public void iniciarDriver()
    {
        setDriver("https://www.demoblaze.com");
    }

    @Test
    public void CompraMacBookProAmex() throws InterruptedException
    {
        CompraMacBookProAmexWorkflow();
    }

    @Test
    public void CompraMacBookProVisa() throws InterruptedException
    {
        CompraMacBookProVisaWorkflow();
    }

    @Test
    public void CompraMacBookProVisaSemCartao() throws InterruptedException
    {
        CompraMacBookProVisaSemCartaoWorkflow();
    }

    @After public void fecharDriver()
    {
        driver.quit();
    }
}
