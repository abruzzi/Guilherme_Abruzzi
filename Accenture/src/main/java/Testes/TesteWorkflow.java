package Testes;

import Pages.Pages.CartPage;
import Pages.Pages.HomePage;
import Pages.Pages.PlaceOrderPage;
import Pages.Pages.ProdutoPage;
import Principal.BasePage;

public class TesteWorkflow extends BasePage {

    HomePage home = new HomePage();
    ProdutoPage produto = new ProdutoPage();
    CartPage cart = new CartPage();
    PlaceOrderPage placeorder = new PlaceOrderPage();


    public void CompraMacBookProAmexWorkflow() throws InterruptedException {

        String nome = "John Doe";
        String country = "Portugal";
        String city = "Lisbon";
        String card = "375567668884617";
        String month = "02";
        String year = "2030";

        home.clicarLaptops();
        home.scrollDownMacBookPro();
        home.clicarMacBookPro();
        produto.clicarAddToCart();
        produto.clicarOkAdicaoProduto();
        cart.clicarCart();
        cart.clicarPlaceOrder();
        String valorMacBookPro = placeorder.getValorMacBookPro();
        placeorder.preencherNome(nome);
        placeorder.preencherCountry(country);
        placeorder.preencherCity(city);
        placeorder.preencherCard(card);
        placeorder.preencherMonth(month);
        placeorder.scrollDownYear();
        placeorder.preencherYear(year);
        placeorder.scrollDownPurchase();
        placeorder.clicarPurchase();
        placeorder.checkId();
        placeorder.checkAmount(valorMacBookPro);
        placeorder.checkName(nome);
    }

    public void CompraMacBookProVisaWorkflow() throws InterruptedException {

        String nome = "Percy Clayton";
        String country = "United States";
        String city = "Jacksonville";
        String card = "4411732769254916";
        String month = "4";
        String year = "2029";

        home.clicarLaptops();
        home.scrollDownMacBookPro();
        home.clicarMacBookPro();
        produto.clicarAddToCart();
        produto.clicarOkAdicaoProduto();
        cart.clicarCart();
        cart.clicarPlaceOrder();
        String valorMacBookPro = placeorder.getValorMacBookPro();
        placeorder.preencherNome(nome);
        placeorder.preencherCountry(country);
        placeorder.preencherCity(city);
        placeorder.preencherCard(card);
        placeorder.preencherMonth(month);
        placeorder.scrollDownYear();
        placeorder.preencherYear(year);
        placeorder.scrollDownPurchase();
        placeorder.clicarPurchase();
        placeorder.checkId();
        placeorder.checkAmount(valorMacBookPro);
        placeorder.checkName(nome);
    }

    public void CompraMacBookProVisaSemCartaoWorkflow() throws InterruptedException {

        String nome = "Percy Clayton";
        String country = "United States";
        String city = "Jacksonville";
        String card = "";
        String month = "4";
        String year = "2029";
        String mensagemCartao = "Please fill out Name and Creditcard.";

        home.clicarLaptops();
        home.scrollDownMacBookPro();
        home.clicarMacBookPro();
        produto.clicarAddToCart();
        produto.clicarOkAdicaoProduto();
        cart.clicarCart();
        cart.clicarPlaceOrder();
        placeorder.preencherNome(nome);
        placeorder.preencherCountry(country);
        placeorder.preencherCity(city);
        placeorder.preencherCard(card);
        placeorder.preencherMonth(month);
        placeorder.scrollDownYear();
        placeorder.preencherYear(year);
        placeorder.scrollDownPurchase();
        placeorder.clicarPurchase();
        placeorder.clicarEVerificarMensagemPedidoDeCartao(mensagemCartao);
    }
}
