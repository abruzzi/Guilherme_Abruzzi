package Pages.Pages;

import Principal.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class ProdutoPage extends BasePage {

    public void clicarAddToCart()
    {
        clicarBotaoXpath("//*[@id='tbodyid']/div[2]/div/a");
    }

    public void clicarOkAdicaoProduto() throws InterruptedException {
        Thread.sleep(5000);
        alertaObterTextoEAceita();
    }
}



