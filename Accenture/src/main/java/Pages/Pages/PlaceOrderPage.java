package Pages.Pages;

import Principal.BasePage;
import junit.framework.AssertionFailedError;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.junit.Assert;

import java.io.Console;
import java.util.Random;
import java.util.logging.LogManager;

import static org.junit.Assert.*;

public class PlaceOrderPage extends BasePage {


   public void preencherNome(String valor)
   {
       escrever("name",valor);
   }

    public void preencherCountry(String valor)
    {
        escrever("country",valor);
    }

    public void preencherCity(String valor)
    {
        escrever("city",valor);
    }
    public void preencherCard(String valor)
    {
        escrever("card",valor);
    }

    public void preencherMonth(String valor)
    {
        escrever("month",valor);
    }

    public void preencherYear(String valor)
    {
        escrever("year",valor);
    }

    public void scrollDownYear()
    {
        WebElement Element = driver.findElement(By.id("year"));
        executarJS("arguments[0].scrollIntoView();",Element);
    }

    public void scrollDownPurchase()
    {
        WebElement Element = driver.findElement(By.xpath("//*[@id='orderModal']/div/div/div[3]/button[2]"));
        executarJS("arguments[0].scrollIntoView();",Element);
    }

    public void clicarPurchase()
    {
        clicarBotaoXpath("//*[@id='orderModal']/div/div/div[3]/button[2]");
    }

    public void checkId()
    {
        assertTrue("Existencia Id" ,obterValorTextoCampoXPath("//*[contains(@class,'lead text-muted')]").contains("Id: "));
    }

    public void checkAmount (String valor) throws InterruptedException
    {
        Thread.sleep(5000);
        assertTrue("Valor do produto" ,obterValorTextoCampoXPath("//*[contains(@class,'lead text-muted')]").contains(valor));
    }

    public void checkName(String valor)
    {
        assertTrue("Nome" ,obterValorTextoCampoXPath("//*[contains(@class,'lead text-muted')]").contains(valor));
    }

    public String getValorMacBookPro()
    {
        return obterTexto("totalm").split("Total: ")[0];
    }

    public void clicarEVerificarMensagemPedidoDeCartao(String valor) throws InterruptedException {
        Thread.sleep(5000);
        Assert.assertEquals(valor,alertaObterTextoEAceita());
    }
}



