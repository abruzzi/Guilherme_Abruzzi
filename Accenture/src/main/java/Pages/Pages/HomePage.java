package Pages.Pages;

import Principal.BasePage;
import org.openqa.selenium.*;

public class HomePage extends BasePage
{


    public void clicarLaptops() throws InterruptedException {
        Thread.sleep(5000);
        clicarBotaoXpath("/html/body/div[5]/div/div[1]/div/a[3]");
    }

    public void scrollDownMacBookPro() throws InterruptedException {
        Thread.sleep(5000);
        WebElement Element = driver.findElement(By.xpath("/html/body/div[5]/div/div[2]/div/div[6]/div/div/h4/a"));
        executarJS("arguments[0].scrollIntoView();", Element);
    }

    public void clicarMacBookPro() throws InterruptedException {
        clicarBotaoXpath("/html/body/div[5]/div/div[2]/div/div[6]/div/div/h4/a");
    }
}



