package Pages.Pages;

import Principal.BasePage;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class CartPage extends BasePage {


    public void clicarCart()
    {
        clicarBotao("cartur");
    }

    public void clicarPlaceOrder()
    {
        clicarBotaoXpath("//*[@id='page-wrapper']/div/div[2]/button");
    }
}



