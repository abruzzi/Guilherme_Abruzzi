1)Para rodar os testes, baixe o projeto do repositório e após abra o Intellij 2020.3.2 preferencialmente.
2)Deve ter instalado o navegador do Google Chrome com uma versão 107.0.5304.88 ou similar com o intuito de não haver problemas de compatibilidade com o driver.
3)Certificar-se de ter instalado o JDK versão 1.8.0_341 instalado em C:\Program Files\Java\jdk1.8.0_341
4)Certificar-se de ter instalado o Maven versão 3.8.6 instalado em C:\Program Files\Maven\apache-maven-3.8.6
5)Rodar o prompt de comando com o seguinte comando javac - deve aparecer algumas informações
6)Rodar o prompt de comando com o seguinte comando mvn - deve aparecer algumas informações
7)Setar a variável de ambiente MAVEN_HOME com o valor C:\Program Files\Maven\apache-maven-3.8.6
8)Setar a variável de ambiente JAVA_HOME com o valor C:\Program Files\Java\jdk1.8.0_341
9)Adicionar na variável Path as variáveis de ambiente C:\Program Files\Maven\bin , %JAVA_HOME%\bin
10)Abrir o Intellij e selecionar a pasta do projeto
11)Abrir a classe Teste do seguinte caminho src\main\java\Testes
12)Executar o teste CompraMacBookProAmex()
13)Executar o teste CompraMacBookProVisa()
14)Executar o teste CompraMacBookProVisaSemCartao()